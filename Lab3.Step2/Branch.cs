﻿using System.Collections.Generic;

namespace Lab3.Step2
{
    class Branch
    {
        public Branch(string town)
        {
            Town = town;
            Dogs = new List<Dog>();
            Cats = new List<Cat>();
        }

        public string Town { get; set; }
        public List<Dog> Dogs { get; set; }
        public List<Cat> Cats { get; set; }

        public List<Animal> GetDogsAsAnimals()
        {
            List<Animal> dogsAsAnimals = new List<Animal>();
            foreach (Dog dog in Dogs)
            {
                dogsAsAnimals.Add(dog);
            }
            return dogsAsAnimals;
        }

        public List<Animal> GetCatsAsAnimals()
        {
            List<Animal> catsAsAnimals = new List<Animal>();
            foreach (Cat cat in Cats)
            {
                catsAsAnimals.Add(cat);
            }
            return catsAsAnimals;
        }
    }
}
