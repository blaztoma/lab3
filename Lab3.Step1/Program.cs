﻿using System;
using System.IO;
using System.Linq;

namespace Lab3.Step1
{
    class Program
    {
        public const int NumberOfBranches = 3;
        public const int MaxNumberOfBreeds = 10;
        public const int MaxNumberOfAnimals = 50;

        static void Main(string[] args)
        {
            Branch[] branches = new Branch[NumberOfBranches];

            branches[0] = new Branch("Kaunas");
            branches[1] = new Branch("Vilnius");
            branches[2] = new Branch("Šiauliai");

            string[] filePaths = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.csv");

            foreach (string path in filePaths)
            {
                ReadAnimalData(path, branches);
            }

            Console.WriteLine("Kaune užregistruoti šunys:");
            PrintAnimalsToConsole(branches[0].Dogs);
            Console.WriteLine();

            Console.WriteLine("Kaune užregistruotos katės:");
            PrintAnimalsToConsole(branches[0].Cats);

            Console.WriteLine();
            Console.WriteLine("Agresyvus Kauno šunys: {0}", CountAggressive(branches[0].Dogs));
            Console.WriteLine("Agresyvus Vilniaus šunys: {0}", CountAggressive(branches[1].Dogs));

            AnimalsContainer kaunasDogs = branches[0].Dogs;
            AnimalsContainer vilniusCats = branches[1].Cats;

            Console.Out.WriteLine("Populiariausia šunų veislė Kaune: {0}", GetMostPopularBreed(kaunasDogs));
            Console.Out.WriteLine("Populiariausia kačių veislė Vilniuje: {0}", GetMostPopularBreed(vilniusCats));
            Console.WriteLine();

            Console.WriteLine("Surūšiuotas visų filialų šunų sąrašas:");
            Console.WriteLine();
            AnimalsContainer allDogs = new AnimalsContainer(Program.MaxNumberOfAnimals * Program.NumberOfBranches);
            for (int i = 0; i < NumberOfBranches; i++)
            {
                for (int j = 0; j < branches[i].Dogs.Count; j++)
                {
                    allDogs.AddAnimal(branches[i].Dogs.GetAnimal(j));
                }
            }
            allDogs.SortAnimals();
            PrintAnimalsToConsole(allDogs);
        }

        static void PrintAnimalsToConsole(AnimalsContainer animals)
        {
            for (int i = 0; i < animals.Count; i++)
            {
                Console.WriteLine("Nr {0,-2}: {1}", (i + 1), animals.GetAnimal(i).ToString());
            }
        }

        private static Branch GetBranchByTown(Branch[] branches, string town)
        {
            for (int i = 0; i < NumberOfBranches; i++)
            {
                if(branches[i].Town == town)
                {
                    return branches[i];
                }
            }
            return null;
        }

        private static void ReadAnimalData(string file, Branch[] branches)
        {

            string town = null;

            using (StreamReader reader = new StreamReader(@file))
            {
                string line = null;
                line = reader.ReadLine();
                if (line != null)
                {
                    town = line;
                }
                Branch branch = GetBranchByTown(branches, town);
                while (null != (line = reader.ReadLine()))
                {
                    string[] values = line.Split(',');
                    char type = Convert.ToChar(line[0]);
                    string name = values[1];
                    int chipId = int.Parse(values[2]);
                    string breed = values[3];
                    string owner = values[4];
                    string phone = values[5];
                    DateTime vd = DateTime.Parse(values[6]);

                    switch (type)
                    {
                        case 'D':
                            //atkreipkite dėmesį - šunys turi papildomą požymį "aggressive"
                            bool aggressive = bool.Parse(values[7]);
                            Dog dog = new Dog(name, chipId, breed, owner, phone, vd, aggressive);
                            if (!branch.Dogs.Contains(dog))
                            {
                                branch.Dogs.AddAnimal(dog);
                            }
                            break;
                        case 'C':
                            Cat cat = new Cat(name, chipId, breed, owner, phone, vd);
                            if (!branch.Cats.Contains(cat))
                            {
                                branch.Cats.AddAnimal(cat);
                            }
                            break;
                    }
                }
            }

        }

        private static void GetBreeds(AnimalsContainer animals, out string[] breeds, out int breedCount)
        {
            breeds = new string[MaxNumberOfBreeds];
            breedCount = 0;

            for (int i = 0; i < animals.Count; i++)
            {
                string breed = animals.GetAnimal(i).Breed;
                if (!breeds.Contains(breed))
                {
                    breeds[breedCount++] = breed;
                }
            }
        }


        private static AnimalsContainer FilterByBreed(AnimalsContainer animals, string breed)
        {
            AnimalsContainer filteredAnimals = new AnimalsContainer(Program.MaxNumberOfAnimals);
            for (int i = 0; i < animals.Count; i++)
            {
                if (animals.GetAnimal(i).Breed == breed)
                {
                    filteredAnimals.AddAnimal(animals.GetAnimal(i));
                }
            }

            return filteredAnimals;
        }

        private static int CountAggressive(AnimalsContainer animals)
        {
            int counter = 0;
            for (int i = 0; i < animals.Count; i++)
            {
                Dog dog = animals.GetAnimal(i) as Dog;
                if (dog != null && dog.Aggressive)
                {
                    counter++;
                }
            }

            return counter;
        }

        private static string GetMostPopularBreed(AnimalsContainer animals)
        {
            String popular = "not found";
            int count = 0;

            int breedCount = 0;
            string[] breeds;

            GetBreeds(animals, out breeds, out breedCount);

            for (int i = 0; i < breedCount; i++)
            {
                AnimalsContainer filteredAnimals = FilterByBreed(animals, breeds[i]);
                if(filteredAnimals.Count > count)
                {
                    popular = breeds[i];
                    count = filteredAnimals.Count;
                }
            }

            return popular;
        }
    }
}
