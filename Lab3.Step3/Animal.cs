﻿using System;

namespace Lab3.Step3
{
    abstract class Animal
    {
        public string Name { get; set; }
        public int ChipId { get; set; }
        public string Breed { get; set; }
        public string Owner { get; set; }
        public string Phone { get; set; }
        public DateTime VaccinationDate { get; set; }

        public Animal(string name, int chipId, string breed, string owner, string phone, DateTime vaccinationDate)
        {
            Name = name;
            ChipId = chipId;
            Breed = breed;
            Owner = owner;
            Phone = phone;
            VaccinationDate = vaccinationDate;
        }

        //metodas paskelbtas abstrakčiu. Tai reiškia, jog vaikų klasės privalės jį realizuoti
        abstract public bool isVaccinationExpired();

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Animal);
        }

        public bool Equals(Animal animal)
        {
            if (Object.ReferenceEquals(animal, null))
            {
                return false;
            }

            if (this.GetType() != animal.GetType())
            {
                return false;
            }

            return (ChipId == animal.ChipId) && (Name == animal.Name);
        }

        public override int GetHashCode()
        {
            return ChipId.GetHashCode() ^ Name.GetHashCode();
        }

    }
}
