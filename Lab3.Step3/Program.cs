﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Lab3.Step3
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, Branch> branches = new Dictionary<string, Branch>();
            branches.Add("Kaunas", new Branch("Kaunas"));
            branches.Add("Vilnius", new Branch("Vilnius"));
            branches.Add("Šiauliai", new Branch("Šiauliai"));

            string[] filePaths = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.csv");

            foreach(string path in filePaths)
            {
                ReadAnimalData(path, branches);
            }

            List<Animal> kaunasDogs = branches["Kaunas"].GetDogsAsAnimals();
            List<Animal> kaunasCats = branches["Kaunas"].GetCatsAsAnimals();

            Console.WriteLine("Kaune užregistruoti šunys:");
            PrintAnimalsToConsole(kaunasDogs);

            Console.WriteLine("Kaune užregistruotos katės:");
            PrintAnimalsToConsole(kaunasCats);

            Console.WriteLine();
            Console.WriteLine("Agresyvus Kauno šunys: {0}", CountAggressive(branches["Kaunas"].Dogs));
            Console.WriteLine("Agresyvus Vilniaus šunys: {0}", CountAggressive(branches["Vilnius"].Dogs));

            Console.WriteLine();
            List<Animal> vilniusCats = branches["Vilnius"].GetCatsAsAnimals();
            Console.Out.WriteLine("Populiariausia šunų veislė Kaune: {0}", GetMostPopularBreed(kaunasDogs));
            Console.Out.WriteLine("Populiariausia kačių veislė Vilniuje: {0}", GetMostPopularBreed(vilniusCats));

            Console.WriteLine();
            Console.WriteLine("Surūšiuotas visų filialų šunų sąrašas:");

            List<Animal> allDogs = new List<Animal>();
            //sudedam visus šunis
            allDogs.AddRange(branches["Vilnius"].GetDogsAsAnimals());
            allDogs.AddRange(branches["Kaunas"].GetDogsAsAnimals());
            allDogs.AddRange(branches["Šiauliai"].GetDogsAsAnimals());

            //rikiuojame
            List<Animal> sortedDogs = kaunasDogs.OrderBy(d => d.Breed).ThenBy(d => d.Name).ToList();
            PrintAnimalsToConsole(sortedDogs);

            Console.Read();
        }

        private static void ReadAnimalData(string file, Dictionary<string, Branch> branches)
        {
            string town = null;

            using (StreamReader reader = new StreamReader(@file))
            {
                string line = null;
                line = reader.ReadLine();
                if (line != null)
                {
                    town = line;
                }
                Branch branch = branches[town];
                while (null != (line = reader.ReadLine()))
                {
                    string[] values = line.Split(',');
                    char type = Convert.ToChar(line[0]);
                    string name = values[1];
                    int chipId = int.Parse(values[2]);
                    string breed = values[3];
                    string owner = values[4];
                    string phone = values[5];
                    DateTime vd = DateTime.Parse(values[6]);

                    switch (type)
                    {
                        case 'D':
                            //atkreipkite dėmesį - šunys turi papildomą požymį "aggressive"
                            bool aggressive = bool.Parse(values[7]);
                            Dog dog = new Dog(name, chipId, breed, owner, phone, vd, aggressive);
                            branch.Dogs.Add(dog);
                            break;
                        case 'C':
                            Cat cat = new Cat(name, chipId, breed, owner, phone, vd);
                            branch.Cats.Add(cat);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Show animal data in the Console
        /// </summary>
        /// <param name="cats">Array of dogs</param>
        /// <param name="count">Count of the cats</param>
        static void PrintAnimalsToConsole(List<Animal> animals)
        {
            foreach (Animal animal in animals)
            {
                Console.WriteLine("{0}", animal.ToString());
            }
        }

        private static List<string> GetBreeds(List<Animal> animals)
        {
            List<string> breeds = new List<string>();
            return breeds = animals.Select(o => o.Breed).Distinct().ToList();
        }

        private static List<Animal> FilterByBreed(List<Animal> animals, string breed)
        {
            List<Animal> filtered = new List<Animal>();
            return animals.FindAll(o => o.Breed.Equals(breed)).ToList();
        }

        private static int CountAggressive(List<Dog> dogs)
        {
            return dogs.Where(o => o.Aggressive.Equals(true)).Count();
        }

        private static string GetMostPopularBreed(List<Animal> animals)
        {
            List<string> popular = animals.GroupBy(o => o.Breed)
                                    .OrderByDescending(op => op.Count())
                                    .Take(1)
                                    .Select(g => g.Key).ToList();
            return popular[0];
        }
    }
}
