﻿using System;

namespace Lab3.Step3
{
    class Cat : Animal
    {
        private static int VaccinationDurationMonths = 6;

        public Cat(string name, int chipId, string breed, string owner, string phone, DateTime vaccinationDate)
            : base(name, chipId, breed, owner, phone, vaccinationDate)
        {
        }

        //abstraktaus Animal klasės metodo realizacija
        public override bool isVaccinationExpired()
        {
            return VaccinationDate.AddMonths(VaccinationDurationMonths).CompareTo(DateTime.Now) > 0;
        }

        public override String ToString()
        {
            return String.Format("Cat - ChipId: {0,5}, Name: {1,10}, Owner: {2,16} ({3}), Last vaccination date: {4:yyyy-MM-dd}", ChipId, Name, Owner, Phone, VaccinationDate);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Cat);
        }

        public bool Equals(Cat cat)
        {
            return base.Equals(cat);
        }

        public override int GetHashCode()
        {
            return ChipId.GetHashCode() ^ Name.GetHashCode();
        }
    }
}
